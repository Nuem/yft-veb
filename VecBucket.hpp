#ifndef VECBUCKET_HPP_INCLUDED
#define VECBUCKET_HPP_INCLUDED
#include <algorithm>
#include <vector>
#include "IBucket.hpp"

using namespace std;

template<typename IntType>
class VecBucket
{
private:
	std::vector<IntType> m_store;
	IntType minimum;
	IntType maximum;
	IntType rep = 0;

public:
	VecBucket(IntType, IntType);
	VecBucket(IntType, IntType, std::vector<IntType>);
	~VecBucket();
	size_t Size();
	void Add(IntType);
	void Remove(IntType);
	void SetStore(std::vector<IntType> v);
	bool Contains(IntType key);
	VecBucket* Split(IntType);
	IntType Pred(IntType);
	IntType Succ(IntType);
	IntType GetRep();
};

template<typename IntType>
VecBucket<IntType>::VecBucket(IntType mi, IntType ma)
{
	minimum = mi;
	maximum = ma;
}

template<typename IntType>
VecBucket<IntType>::VecBucket(IntType mi, IntType ma, vector<IntType> v)
{
	minimum = mi;
	maximum = ma;
	m_store = v;
}

template<typename IntType>
VecBucket<IntType>::~VecBucket()
{
}

template<typename IntType>
size_t VecBucket<IntType>::Size()
{
	return m_store.size();
}

template<typename IntType>
void VecBucket<IntType>::Add(IntType key)
{
	m_store.push_back(key);
}

// Remove certain element from bucket
template<typename IntType>
void VecBucket<IntType>::Remove(IntType key)
{
	auto p = find(m_store.begin(), m_store.end(), key);
	if (p != m_store.end()) m_store.erase(p);
}

template<typename IntType>
void VecBucket<IntType>::SetStore(std::vector<IntType> v)
{
	m_store = v;
}

template<typename IntType>
bool VecBucket<IntType>::Contains(IntType key)
{
	return (std::find(m_store.begin(), m_store.end(), key) != m_store.end());
}

template<typename IntType>
VecBucket<IntType>* VecBucket<IntType>::Split(IntType pos)
{
	// Sort the vector before splitting it
	sort(m_store.begin(), m_store.end());

	typename vector<IntType>::size_type b_size = m_store.size();
	size_t const half_size = b_size / 2;
	vector<IntType>low = vector<IntType>(m_store.begin(), m_store.begin() + half_size);
	vector<IntType> high(m_store.begin() + half_size, m_store.end());
	VecBucket<IntType>* v = new VecBucket(minimum, maximum, high);
	m_store = low;
	return v;
}

template<typename IntType>
IntType VecBucket<IntType>::Pred(IntType key)
{
	IntType candidate = key;
	vector::iterator it = m_store.begin();

	// Find first value smaller than key
	while (it != m_store.end() && *it >= key)
	{
		++it;
	}

	// No smaller value found! Pres is identity.
	if (it == m_store.end()) return key;

	// Find largest value smaller than key in the rest.
	candidate = *it;
	while (it != m_store.end())
	{
		if (*it > candidate && *it < key) candidate = *it;
		++it;
	}
	return candidate;
}

template<typename IntType>
IntType VecBucket<IntType>::Succ(IntType key)
{
	// worst case O(n), n = bucket size
	IntType candidate = key;
	typename vector<IntType>::iterator it = m_store.begin();

	// Find first value bigger than key
	while (it != m_store.end() && *it <= key)
	{
		++it;
	}

	// No bigger value found! Succ is identity.
	if (it == m_store.end()) return key;

	// Find smallest value bigger than key in the rest
	candidate = *it;
	while (it != m_store.end())
	{
		if (*it < candidate && *it > key) candidate = *it;
		++it;
	}
	return candidate;
}

template<typename IntType>
IntType VecBucket<IntType>::GetRep()
{
	//TODO: Select rep logic! Rep may change as elements are added or removed!
	return m_store[rep];
}
#endif // !VECBUCKET_HPP_INCLUDED
