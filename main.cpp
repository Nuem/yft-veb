#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <random>
#include <string>
//#include <ctime>
//#include <cstdlib>
//#include "VecBucket.hpp"
#include "YFT.hpp"
//#include "XFT.hpp"
//#include "XFTNode.hpp"
#include "vEBT.hpp"
#include "avEBT.hpp"

#define integer_type unsigned long int

using namespace std;


bool chance()
{
	return (rand() % 2) == 1;
}


int main()
{
	// Common stuff
	vector<integer_type> a;
	vector<integer_type> b;
	integer_type number_of_ops;
	integer_type max_number_of_items = 16000000;
	integer_type number_of_items = 4000000;
	integer_type* c;
	integer_type start_ops = 10000000;
	integer_type max_ops = 10000000;
	integer_type i = 0;
	integer_type min_bucket_size = 16;
	integer_type max_bucket_size = 64;
	integer_type byte_count = sizeof(integer_type) * 8;
	integer_type testnum = 0;
	integer_type resultnum = 0;
	integer_type contains_counter = 0;
	string struct_name;
	string op_name;
	bool noerror = true;
	bool testbool = true;
	bool test_vec = false;
	bool test_yft = false;
	bool test_vebt = false;
	bool test_avebt = true;
	

	// File output
	ofstream output_file;
	string tvec = (test_vec) ? "Arr_" : "";
	string tyft = (test_yft) ? "YFT_" : "";
	string tvebt = (test_vebt) ? "vEBT_" : "";
	string tavebt = (test_avebt) ? "avEBT_" : "";
	string filename = "result_" +tvec + tyft + tvebt + tavebt + to_string(byte_count) + ".csv";
	output_file.open(filename);
	output_file << "Structure, Bits, Operations, Operation, Time" << endl;
	

	// Random stuff
	integer_type u_size = -1;
	integer_type ran_median = u_size / 2;
	integer_type ran_sigma_0 = ran_median / 2;
	std::random_device ran_dev;
	std::mt19937 generator(ran_dev());
	std::normal_distribution<> n_dist(ran_median, ran_sigma_0);


	// Init time measurement
	std::chrono::steady_clock::time_point start;
	std::chrono::steady_clock::time_point stop;
	long long seconds = 0;

	
	while (number_of_items <= max_number_of_items)
	{
		// Array as baseline comparison
		if (test_vec)
		{
			c = new integer_type[number_of_items];
			number_of_ops = start_ops;
			struct_name = "Array";
			op_name = "Insert";
			start = chrono::steady_clock::now();
			while (i < number_of_items)
			{
				c[i] = n_dist(generator);
				i++;
			}
			stop = chrono::steady_clock::now();
			seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
			output_file << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			cout << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			i = 0;

			while (number_of_ops <= max_ops)
			{
				op_name = "Contains";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = n_dist(generator);
					integer_type pos = 0;
					while (pos < number_of_items)
					{
						if (c[pos] == testnum)
						{
							noerror == true;
							break;
						}
						pos++;
					}
					if (pos < number_of_items)
					{
						contains_counter++;
						testbool |= noerror;
					}
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Predecessor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = n_dist(generator);
					integer_type pos = 0;
					integer_type candidate = 0;
					while (pos < number_of_items)
					{
						if (c[pos] <= testnum)
						{
							noerror == true;
							candidate = c[pos];
							break;
						}
						pos++;
					}
					while (pos < number_of_items)
					{
						if (c[pos] > candidate && c[pos] <= testnum)
						{
							candidate = c[pos];
						}
						pos++;
					}
					resultnum += candidate;
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Successor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = n_dist(generator);
					integer_type pos = 0;
					integer_type candidate = 0;
					while (pos < number_of_items)
					{
						if (c[pos] >= testnum)
						{
							noerror == true;
							candidate = c[pos];
							break;
						}
						pos++;
					}
					while (pos < number_of_items)
					{
						if (c[pos] < candidate && c[pos] >= testnum)
						{
							candidate = c[pos];
						}
						pos++;
					}
					resultnum += candidate;
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				a.clear();

				delete[] c;
				number_of_ops *= 2;
			}
		}

		// Y-Fast Trie
		if (test_yft)
		{
			YFT<integer_type> yft;
			number_of_ops = start_ops;
			struct_name = "Y-Fast-Trie";
			op_name = "Insert";
			start = chrono::steady_clock::now();
			while (i < number_of_items)
			{
				yft.Insert(n_dist(generator));
				i++;
			}
			stop = chrono::steady_clock::now();
			seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
			output_file << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			cout << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			i = 0;
			while (number_of_ops <= max_ops)
			{
				op_name = "Contains";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testbool &= yft.Contains(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Predecessor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					resultnum += yft.Pred(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Successor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					resultnum += yft.Succ(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				yft.Clear();

				number_of_ops *= 2;
			}

		}

		// van Emde Boas tree (hash)
		if (test_vebt)
		{
			vEBT<integer_type> vebt;
			number_of_ops = start_ops;
			struct_name = "vEB-Tree (Hash)";
			op_name = "Insert";
			start = chrono::steady_clock::now();
			while (i < number_of_items)
			{
				vebt.Insert(n_dist(generator));
				i++;
			}
			stop = chrono::steady_clock::now();
			seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
			output_file << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			cout << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			i = 0;
			while (number_of_ops <= max_ops)
			{
				op_name = "Contains";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					noerror = vebt.Contains(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Predecessor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = vebt.Pred(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Successor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = vebt.Succ(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				vebt.Clear();

				number_of_ops *= 2;
			}
		}

		// van Emde Boas tree (array)
		if (test_avebt)
		{
			avEBT<integer_type> avebt;
			number_of_ops = start_ops;
			struct_name = "vEB-Tree (Array)";
			op_name = "Insert";
			start = chrono::steady_clock::now();
			while (i < number_of_items)
			{
				avebt.Insert(n_dist(generator));
				i++;
			}
			stop = chrono::steady_clock::now();
			seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
			output_file << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			cout << struct_name << "," << byte_count << "," << number_of_items << "," << op_name << "," << seconds << endl;
			i = 0;
			while (number_of_ops <= max_ops)
			{
				op_name = "Contains";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					noerror = avebt.Contains(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Predecessor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = avebt.Pred(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				op_name = "Successor";
				start = chrono::steady_clock::now();
				while (i < number_of_ops)
				{
					testnum = avebt.Succ(n_dist(generator));
					i++;
				}
				stop = chrono::steady_clock::now();
				seconds = chrono::duration_cast<chrono::seconds>(stop - start).count();
				output_file << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				cout << struct_name << "," << byte_count << "," << number_of_ops << "," << op_name << "," << seconds << endl;
				i = 0;

				avebt.Clear();

				number_of_ops *= 2;
			}
		}

		number_of_items *= 2;
	}
	
	
	// Close file
	output_file.close();
	cout << "Resultnum: " << resultnum << ", Testbool: " << testbool << ", Contains_counter: " << contains_counter << endl;
	return 0;
}


