#pragma once

template<typename IntType>
struct vEBNode
{
        IntType minimum;
        IntType maximum;
        IntType u;
        vEBNode<IntType> *summary = nullptr; // single vEBNode (possible root of a tree)
		std::unordered_map<IntType, vEBNode<IntType>*> cluster;
};
