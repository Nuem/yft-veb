#pragma once
#include <vector>
#include "avEBNode.hpp"
#define NIL (IntType)-1

template<typename IntType>
class avEBT
{
public:
	avEBT();
	~avEBT();
	void Clear();
	void Insert(IntType);
	void Delete(IntType);
	bool Contains(IntType);
	IntType Minimum();
	IntType Maximum();
	IntType Pred(IntType);
	IntType Succ(IntType);
	IntType Size();

private:
	avEBNode<IntType>* Root;
	avEBNode<IntType>* BuildTree(IntType u);
	bool Member(avEBNode<IntType>* v, IntType);
	IntType my_size;
	IntType SuccMember(avEBNode<IntType>* v, IntType);
	IntType PredMember(avEBNode<IntType>* v, IntType);
	void EmptyInsertMember(avEBNode<IntType>* v, IntType);
	void InsertMember(avEBNode<IntType>* v, IntType);
	void DeleteMember(avEBNode<IntType>* v, IntType);
	void DeleteNodeRec(avEBNode<IntType>* v);
	avEBNode<IntType>* GetSummary(avEBNode<IntType>* v);
	avEBNode<IntType>* GetFromCluster(avEBNode<IntType>* v, IntType idx);
	IntType high(avEBNode<IntType>* v, IntType);
	IntType low(avEBNode<IntType>* v, IntType);
	IntType index(avEBNode<IntType>* v, IntType, IntType);
	IntType minimum(avEBNode<IntType>* v);
	IntType maximum(avEBNode<IntType>* v);
	IntType LSQRT(IntType);
	IntType USQRT(IntType x);
	//IntType RoundSquare(IntType x);
	
};

template<typename IntType>
avEBT<IntType>::avEBT()
{
	// IntType must be of unsigned type! 
	IntType u_size =  -1;
	Root = BuildTree(u_size);
	my_size = 0;
}

template<typename IntType>
avEBT<IntType>::~avEBT()
{
	Clear();
	delete[] Root->cluster;
	delete Root;
}

template<typename IntType>
void avEBT<IntType>::Clear()
{
	DeleteNodeRec(Root);
	delete Root;
	Root = BuildTree(-1);
}

template<typename IntType>
IntType avEBT<IntType>::Minimum()
{
	return minimum(Root);
}

template<typename IntType>
IntType avEBT<IntType>::Maximum()
{
	return maximum(Root);
}

template<typename IntType>
IntType avEBT<IntType>::minimum(avEBNode<IntType>* v)
{
	return v->minimum;
}

template<typename IntType>
IntType avEBT<IntType>::maximum(avEBNode<IntType>* v)
{
	return v->maximum;
}

template<typename IntType>
bool avEBT<IntType>::Contains(IntType x)
{
	return Member(Root, x);
}

template<typename IntType>
void avEBT<IntType>::Insert(IntType key)
{
	if (!Contains(key))
	{
		InsertMember(Root, key);
		my_size++;
	}
}

template<typename IntType>
void avEBT<IntType>::Delete(IntType key)
{
	if (Contains(key))
	{
		DeleteMember(Root, key);
		my_size--;
	}
}

template<typename IntType>
bool avEBT<IntType>::Member(avEBNode<IntType>* v, IntType x)
{
	if (x == v->minimum || x == v->maximum)
		return true;
	else if (v->u <= 2)
		return false;
	else
	{
		IntType h = high(v, x);
		if (v->cluster[h] == nullptr)
		{
			return false;
		}
		else
		{
			avEBNode<IntType>* tmp = v->cluster[h];
			IntType l = low(v, x);
			return Member(tmp, l);
		}
	}
}

template<typename IntType>
IntType avEBT<IntType>::Succ(IntType key)
{
	return SuccMember(Root, key);
}

template<typename IntType>
IntType avEBT<IntType>::Size()
{
	return my_size;
}

template<typename IntType>
IntType avEBT<IntType>::Pred(IntType key)
{
	return PredMember(Root, key);
}

template<typename IntType>
IntType avEBT<IntType>::SuccMember(avEBNode<IntType>* v, IntType x)
{
	if (v->u == 2)
	{
		if (x == 0 && v->maximum == 1)
			return 1;
		else
			return NIL;
	}
	else if (v->minimum != NIL && x < v->minimum)
		return v->minimum;
	else
	{
		IntType maxlow = NIL;
		if (v->cluster[high(v, x)] != nullptr)
		{
			maxlow = maximum(v->cluster[high(v, x)]);
		}
		if (maxlow != NIL && low(v, x) < maxlow)
		{
			IntType offset = NIL;
			if (v->cluster[high(v, x)] != nullptr)
			{
				offset = SuccMember(v->cluster[high(v, x)], low(v, x));
			}
			return index(v, high(v, x), offset);
		}
		else
		{
			IntType succ_cluster = NIL;
			if (v->summary != nullptr)
			{
				succ_cluster = SuccMember(v->summary, high(v, x));
			}
			if (succ_cluster == NIL)
				return NIL;
			else
			{
				IntType offset = NIL;
				if (v->cluster[succ_cluster] != nullptr)
				{
					offset = minimum(v->cluster[succ_cluster]);
				}
				return index(v, succ_cluster, offset);
			}
		}
	}
}

template<typename IntType>
IntType avEBT<IntType>::PredMember(avEBNode<IntType>* v, IntType x)
{
	if (v->u == 2)
	{
		if (x == 1 && v->minimum == 0)
			return 0;
		else return NIL;
	}
	else if (v->maximum != NIL && x > v->maximum)
		return v->maximum;
	else
	{
		IntType minlow = NIL;
		if (v->cluster[high(v, x)] != nullptr)
		{
			minlow = minimum(v->cluster[high(v, x)]);
		}
		if (minlow != NIL && low(v, x) > minlow)
		{
			IntType offset = NIL;
			if (v->cluster[high(v, x)] != nullptr)
			{
				offset = PredMember(v->cluster[high(v, x)], low(v, x));
			}
			return index(v, high(v, x), offset);
		}
		else
		{
			IntType pred_cluster = NIL;
			if (v->summary != nullptr)
			{
				pred_cluster = PredMember(v->summary, high(v, x));
			}
			if (pred_cluster == NIL)
			{
				if (v->minimum != NIL && x > v->minimum)
					return v->minimum;
				else return NIL;
			}
			else
			{
				IntType offset = NIL;
				if (v->cluster[pred_cluster] != nullptr)
				{
					offset = maximum(v->cluster[pred_cluster]);
				}
				return index(v, pred_cluster, offset);
			}
		}
	}
}

template<typename IntType>
void avEBT<IntType>::EmptyInsertMember(avEBNode<IntType>* v, IntType x)
{
	v->minimum = x;
	v->maximum = x;
}

template<typename IntType>
void avEBT<IntType>::InsertMember(avEBNode<IntType>* v, IntType x)
{
	if (v->minimum == NIL)
		EmptyInsertMember(v, x);
	else
	{
		if (x < v->minimum)
		{
			IntType tmp = v->minimum;
			v->minimum = x;
			x = tmp;
		}
		if (v->u > 2)
		{
			if ((GetFromCluster(v, high(v, x)))->minimum == NIL)
			{
				InsertMember(GetSummary(v), high(v, x));
				EmptyInsertMember(GetFromCluster(v, high(v, x)), low(v, x));
			}
			else
				InsertMember(GetFromCluster(v, high(v, x)), low(v, x));
		}
		if (x > v->maximum)
			v->maximum = x;
	}
}

template<typename IntType>
void avEBT<IntType>::DeleteMember(avEBNode<IntType>* v, IntType x)
{
	if (v->minimum == v->maximum)
	{
		// Empty node
		v->minimum = NIL;
		v->maximum = NIL;

		// TODO: Free memory! Remove all children.
		/*delete (v.summary);
		v.summary = nullptr;
		IntType num_children = RoundSquare(v.u);
		for (IntType i = 0; i < num_children; i++)
		{
			if (v.cluster[i] != nullptr)
				delete[] v.cluster[i]->cluster;
			delete (v.cluster[i]);
			v.cluster[i] = nullptr;
		}*/
		DeleteNodeRec(v);
		//delete(&v);
	}
	else if (v->u == 2)
	{
		if (x == 0)
			v->minimum = 1;
		else
			v->minimum = 0;
		v->maximum = v->minimum;
	}
	else
	{
		if (x == v->minimum)
		{
			IntType firstcluster = minimum(GetSummary(v));
			x = index(v, firstcluster, GetFromCluster(v, firstcluster)->minimum);
			v->minimum = x;
		}
		avEBNode<IntType>* tmp = GetFromCluster(v, high(v, x));
		DeleteMember(tmp, low(v, x));
		if ((GetFromCluster(v, high(v, x)))->minimum == NIL)
		{
			DeleteMember(GetSummary(v), high(v, x));
			if (x == v->maximum)
			{
				IntType summary_max = maximum(GetSummary(v));
				if (summary_max == NIL)
					v->maximum = v->minimum;
				else
					v->maximum = index(v, summary_max, (GetFromCluster(v, summary_max))->maximum);
			}
		}
		else if (x == v->maximum)
			v->maximum = index(v, high(v, x), (GetFromCluster(v, high(v, x)))->maximum);
	}
}

template<typename IntType>
void avEBT<IntType>::DeleteNodeRec(avEBNode<IntType>* v)
{
	if (v->summary != nullptr)
	{
		DeleteNodeRec(v->summary);
		v->summary = nullptr;
	}

	IntType num_children = USQRT(v->u);
	for (IntType i = 0; i < num_children; i++)
	{
		if (v->cluster[i] != nullptr)
		{
			DeleteNodeRec(v->cluster[i]);
			delete[] v->cluster[i]->cluster;
			delete (v->cluster[i]);
			v->cluster[i] = nullptr;
		}
	}
}

template<typename IntType>
avEBNode<IntType>* avEBT<IntType>::BuildTree(IntType u)
{
	IntType u_square = USQRT(u);
	avEBNode<IntType>* v = new avEBNode<IntType>();
	v->u = u;
	v->minimum = NIL;
	v->maximum = NIL;
	v->cluster = new avEBNode<IntType>*[u_square]();
	for (IntType i = 0; i < u_square; i++)
	{
		v->cluster[i] = nullptr;
	}
	return v;
}

template<typename IntType>
avEBNode<IntType>* avEBT<IntType>::GetFromCluster(avEBNode <IntType>* v, IntType idx)
{
	// If the node doesn't exist yet in the cluster create it
	if (v->cluster[idx] == nullptr)
	{
		avEBNode<IntType>* v_new = BuildTree(LSQRT(v->u));
		v->cluster[idx] = v_new;
	}
	return v->cluster[idx];
}


template<typename IntType>
avEBNode<IntType>* avEBT<IntType>::GetSummary(avEBNode<IntType>* v)
{
	// If the summary node doesn't yet exist create it.
	if (v->summary == nullptr)
	{
		v->summary = BuildTree(USQRT(v->u));
	}
	return v->summary;
}

template<typename IntType>
IntType avEBT<IntType>::high(avEBNode<IntType>* v, IntType x)
{
	return (IntType)(floor(x / USQRT(v->u)));
}

template<typename IntType>
IntType avEBT<IntType>::low(avEBNode<IntType>* v, IntType x)
{
	return (IntType)(x % USQRT(v->u));
}

template<typename IntType>
IntType avEBT<IntType>::index(avEBNode<IntType>* v, IntType x, IntType y)
{
	return (IntType)(x * USQRT(v->u) + y);
}

template<typename IntType>
inline IntType avEBT<IntType>::LSQRT(IntType x)
{
	return (IntType)(pow(2, floor(log2(x) / 2)));
}

template<typename IntType>
inline IntType avEBT<IntType>::USQRT(IntType x)
{
	return (IntType)(pow(2, ceil(log2(x) / 2)));
}

//template<typename IntType>
//IntType avEBT<IntType>::RoundSquare(IntType x)
//{
//	return (IntType)roundl(sqrt(x));
//}
