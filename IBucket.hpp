#ifndef IBUCKET_HPP_INCLUDED
#define IBUCKET_HPP_INCLUDED
class IBucket
{
protected:
	IBucket() {}

public:
	virtual ~IBucket() {}
	virtual size_t Size() = 0;
	virtual void Add(unsigned int) = 0;
	virtual void Remove(unsigned int) = 0;
	virtual IBucket* Split(int) = 0;
	virtual unsigned int Pred(unsigned int) = 0;
	virtual unsigned int Succ(unsigned int) = 0;
	virtual unsigned int GetRep() = 0;
};


#endif // IBUCKET_HPP_INCLUDED

