#pragma once
#include <vector>
#include "vEBNode.hpp"
#define NIL (IntType)-1

template<typename IntType>
class vEBT
{
public:
	vEBT();
	~vEBT();
	void Clear();
	void Insert(IntType);
	void Delete(IntType);
	bool Contains(IntType);
	IntType Minimum();
	IntType Maximum();
	IntType Pred(IntType);
	IntType Succ(IntType);
	IntType Size();

private:
	vEBNode<IntType>* Root;
	vEBNode<IntType>* BuildTree(IntType u);
	bool Member(vEBNode<IntType>* v, IntType);
	IntType my_size;
	IntType SuccMember(vEBNode<IntType>* v, IntType);
	IntType PredMember(vEBNode<IntType>* v, IntType);
	void EmptyInsertMember(vEBNode<IntType>* v, IntType);
	void InsertMember(vEBNode<IntType>* v, IntType);
	void DeleteMember(vEBNode<IntType>* v, IntType);
	void DeleteNodeRec(vEBNode<IntType>* v);
	vEBNode<IntType>* GetSummary(vEBNode<IntType>* v);
	vEBNode<IntType>* GetFromCluster(vEBNode<IntType>* v, IntType idx);
	IntType high(vEBNode<IntType>* v, IntType);
	IntType low(vEBNode<IntType>* v, IntType);
	IntType index(vEBNode<IntType>* v, IntType, IntType);
	IntType minimum(vEBNode<IntType>* v);
	IntType maximum(vEBNode<IntType>* v);
	IntType LSQRT(IntType);
	IntType USQRT(IntType x);

};

template<typename IntType>
vEBT<IntType>::vEBT()
{
	// IntType must be of unsigned type! 
	IntType u_size = -1;
	Root = BuildTree(u_size);
	my_size = 0;
}

template<typename IntType>
vEBT<IntType>::~vEBT()
{
	Clear();
	Root->cluster.clear();
	delete Root;
}

template<typename IntType>
void vEBT<IntType>::Clear()
{
	DeleteNodeRec(Root);
	delete Root;
	Root = BuildTree(-1);
}

template<typename IntType>
IntType vEBT<IntType>::Minimum()
{
	return minimum(Root);
}

template<typename IntType>
IntType vEBT<IntType>::Maximum()
{
	return maximum(Root);
}

template<typename IntType>
IntType vEBT<IntType>::minimum(vEBNode<IntType>* v)
{
	return v->minimum;
}

template<typename IntType>
IntType vEBT<IntType>::maximum(vEBNode<IntType>* v)
{
	return v->maximum;
}

template<typename IntType>
bool vEBT<IntType>::Contains(IntType x)
{
	return Member(Root, x);
}

template<typename IntType>
void vEBT<IntType>::Insert(IntType key)
{
	if (!Contains(key))
	{
		InsertMember(Root, key);
		my_size++;
	}
}

template<typename IntType>
void vEBT<IntType>::Delete(IntType key)
{
	if (Contains(key))
	{
		DeleteMember(Root, key);
		my_size--;
	}
}

template<typename IntType>
bool vEBT<IntType>::Member(vEBNode<IntType>* v, IntType x)
{
	if (x == v->minimum || x == v->maximum)
		return true;
	else if (v->u <= 2)
		return false;
	else
	{
		IntType h = high(v, x);
		auto it = v->cluster.find(h);
		if (it == v->cluster.end()) return false;
		vEBNode<IntType>* node = it->second;
		IntType l = low(v, x);
		return Member(node, l);
	}
}

template<typename IntType>
IntType vEBT<IntType>::Succ(IntType key)
{
	return SuccMember(Root, key);
}

template<typename IntType>
IntType vEBT<IntType>::Size()
{
	return my_size;
}

template<typename IntType>
IntType vEBT<IntType>::Pred(IntType key)
{
	return PredMember(Root, key);
}

template<typename IntType>
IntType vEBT<IntType>::SuccMember(vEBNode<IntType>* v, IntType x)
{
	if (v->u == 2)
	{
		if (x == 0 && v->maximum == 1)
			return 1;
		else
			return NIL;
	}
	else if (v->minimum != NIL && x < v->minimum)
		return v->minimum;
	else
	{
		IntType maxlow = maximum(GetFromCluster(v, high(v, x)));
		if (maxlow != NIL && low(v, x) < maxlow)
		{
			IntType offset = SuccMember(GetFromCluster(v, high(v, x)), low(v, x));
			return index(v, high(v, x), offset);
		}
		else
		{
			IntType succ_cluster = SuccMember(GetSummary(v), high(v, x));
			if (succ_cluster == NIL)
				return NIL;
			else
			{
				IntType offset = minimum(GetFromCluster(v, succ_cluster));
				return index(v, succ_cluster, offset);
			}
		}
	}
}

template<typename IntType>
IntType vEBT<IntType>::PredMember(vEBNode<IntType>* v, IntType x)
{
	if (v->u == 2)
	{
		if (x == 1 && v->minimum == 0)
			return 0;
		else return NIL;
	}
	else if (v->maximum != NIL && x > v->maximum)
		return v->maximum;
	else
	{
		IntType minlow = minimum(GetFromCluster(v, high(v, x)));
		if (minlow != NIL && low(v, x) > minlow)
		{
			IntType offset = PredMember(GetFromCluster(v, high(v, x)), low(v, x));
			return index(v, high(v, x), offset);
		}
		else
		{
			IntType predcluster = PredMember(GetSummary(v), high(v, x));
			if (predcluster == NIL)
			{
				if (v->minimum != NIL && x > v->minimum)
					return v->minimum;
				else return NIL;
			}
			else
			{
				IntType offset = maximum(GetFromCluster(v, predcluster));
				return index(v, predcluster, offset);
			}
		}
	}
}

template<typename IntType>
void vEBT<IntType>::EmptyInsertMember(vEBNode<IntType>* v, IntType x)
{
	v->minimum = x;
	v->maximum = x;
}

template<typename IntType>
void vEBT<IntType>::InsertMember(vEBNode<IntType>* v, IntType x)
{
	if (v->minimum == NIL)
		EmptyInsertMember(v, x);
	else
	{
		if (x < v->minimum)
		{
			IntType tmp = v->minimum;
			v->minimum = x;
			x = tmp;
		}
		if (v->u > 2)
		{
			if ((GetFromCluster(v, high(v, x)))->minimum == NIL)
			{
				InsertMember(GetSummary(v), high(v, x));
				EmptyInsertMember(GetFromCluster(v, high(v, x)), low(v, x));
			}
			else
				InsertMember(GetFromCluster(v, high(v, x)), low(v, x));
		}
		if (x > v->maximum)
			v->maximum = x;
	}
}

template<typename IntType>
void vEBT<IntType>::DeleteMember(vEBNode<IntType>* v, IntType x)
{
	if (v->minimum == v->maximum)
	{
		// Empty node
		v->minimum = NIL;
		v->maximum = NIL;

		// TODO: Free memory! Remove all children.
		/*delete (v.summary);
		v.summary = nullptr;
		IntType num_children = RoundSquare(v.u);
		for (IntType i = 0; i < num_children; i++)
		{
			if (v.cluster[i] != nullptr)
				delete[] v.cluster[i]->cluster;
			delete (v.cluster[i]);
			v.cluster[i] = nullptr;
		}*/
		DeleteNodeRec(v);
		//delete(&v);
	}
	else if (v->u == 2)
	{
		if (x == 0)
			v->minimum = 1;
		else
			v->minimum = 0;
		v->maximum = v->minimum;
	}
	else
	{
		if (x == v->minimum)
		{
			IntType firstcluster = minimum(GetSummary(v));
			x = index(v, firstcluster, GetFromCluster(v, firstcluster)->minimum);
			v->minimum = x;
		}
		vEBNode<IntType>* tmp = GetFromCluster(v, high(v, x));
		DeleteMember(tmp, low(v, x));
		if ((GetFromCluster(v, high(v, x)))->minimum == NIL)
		{
			DeleteMember(GetSummary(v), high(v, x));
			if (x == v->maximum)
			{
				IntType summary_max = maximum(GetSummary(v));
				if (summary_max == NIL)
					v->maximum = v->minimum;
				else
					v->maximum = index(v, summary_max, (GetFromCluster(v, summary_max))->maximum);
			}
		}
		else if (x == v->maximum)
			v->maximum = index(v, high(v, x), (GetFromCluster(v, high(v, x)))->maximum);
	}
}

template<typename IntType>
void vEBT<IntType>::DeleteNodeRec(vEBNode<IntType>* v)
{
	if (v->summary != nullptr)
	{
		DeleteNodeRec(v->summary);
		v->summary = nullptr;
	}

	IntType num_children = USQRT(v->u);
	auto got = v->cluster.begin();
	while (got != v->cluster.end())
	{
		DeleteNodeRec(got->second);
		delete got->second;
		got = v->cluster.erase(got);
	}
}

template<typename IntType>
vEBNode<IntType>* vEBT<IntType>::BuildTree(IntType u)
{
	IntType u_square = USQRT(u);
	vEBNode<IntType>* v = new vEBNode<IntType>();
	v->u = u;
	v->minimum = NIL;
	v->maximum = NIL;
	return v;
}

template<typename IntType>
vEBNode<IntType>* vEBT<IntType>::GetFromCluster(vEBNode <IntType>* v, IntType idx)
{
	// If the node doesn't exist yet in the cluster create it
	auto got = v->cluster.find(idx);
	if (v->cluster.find(idx) == v->cluster.end())
	{
		vEBNode<IntType>* v_new = BuildTree(LSQRT(v->u));
		v->cluster.insert({ idx, v_new });
		return v_new;
	}
	return got->second;
}

template<typename IntType>
vEBNode<IntType>* vEBT<IntType>::GetSummary(vEBNode<IntType>* v)
{
	// If the summary node doesn't yet exist create it.
	if (v->summary == nullptr)
	{
		v->summary = BuildTree(USQRT(v->u));
	}
	return v->summary;
}

template<typename IntType>
IntType vEBT<IntType>::high(vEBNode<IntType>* v, IntType x)
{
	return (IntType)(floor(x / USQRT(v->u)));
}

template<typename IntType>
IntType vEBT<IntType>::low(vEBNode<IntType>* v, IntType x)
{
	return (IntType)(x % USQRT(v->u));
}

template<typename IntType>
IntType vEBT<IntType>::index(vEBNode<IntType>* v, IntType x, IntType y)
{
	return (IntType)(x * USQRT(v->u) + y);
}

template<typename IntType>
inline IntType vEBT<IntType>::LSQRT(IntType x)
{
	return (IntType)(pow(2, floor(log2(x) / 2)));
}

template<typename IntType>
inline IntType vEBT<IntType>::USQRT(IntType x)
{
	return (IntType)(pow(2, ceil(log2(x) / 2)));
}
