#include <unordered_map>
#include <vector>
#include <iterator>
#include <limits>
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
#include "XFTNode.hpp"

using namespace std;

template<typename IntType, typename ValType>
class XFT
{
	public:
		XFT();
		~XFT();
		void Clear();
		void Insert(IntType, ValType);
		void Delete(IntType);
		void Test_PrintList();
		bool Contains(IntType);
		std::pair<IntType, ValType> Pred(IntType);
		std::pair<IntType, ValType> Succ(IntType);
		std::pair<IntType, ValType> Find(IntType);
		XFTNode<IntType, ValType>* PredNode(IntType);
		XFTNode<IntType, ValType>* SuccNode(IntType);
		IntType GetPrefix(IntType,IntType);
		size_t size();
		XFTNode<IntType, ValType> Minimum;
		XFTNode<IntType, ValType> Maximum;
	private:
		std::unordered_map<IntType,XFTNode<IntType, ValType>*>* Hash;
		XFTNode<IntType, ValType>* PredOrSucc(IntType);
		XFTNode<IntType, ValType> Root;
		int LayerCount;
};

template<typename IntType, typename ValType>
XFT<IntType, ValType>::XFT()
{
	LayerCount = sizeof(IntType) * 8;
	IntType maxval = - 1;
	Hash = new unordered_map<IntType, XFTNode<IntType, ValType>*>[LayerCount];
	Minimum.key = 0;
	Minimum.val = 0;
	Maximum.key = maxval;

	Minimum.succ = &Maximum;
	Maximum.pred = &Minimum;
}

template<typename IntType, typename ValType>
XFT<IntType, ValType>::~XFT()
{
	Clear();
}

// Attention! Assumes that ValType is of type VecBucket*
template<typename IntType, typename ValType>
void XFT<IntType, ValType>::Clear()
{
	typename std::unordered_map<IntType, XFTNode<IntType, ValType>*>::iterator got;
	XFTNode<IntType, ValType>* node;
	VecBucket<IntType>* bucket;
	for (int i = 0; i < LayerCount; i++)
	{
		for (got = Hash[i].begin(); got != Hash[i].end(); got++)
		{
			node = got->second;
			bucket = (VecBucket<IntType>*)(node->val);
			if (bucket != nullptr)
			{
				delete bucket;
			}
			delete node;
		}
		Hash[i].clear();
	}
	Root.desc = nullptr;
	Root.lc = nullptr;
	Root.rc = nullptr;
	Minimum.succ = &Maximum;
	Maximum.pred = &Minimum;
}

template<typename IntType, typename ValType>
void XFT<IntType, ValType>::Test_PrintList()
{
	stringstream ss;
	ss << "Layer 0 list: ";
	XFTNode<IntType> *pn = &Minimum;
	do
	{
		ss << pn->key << ", ";
		pn = pn->succ;
	} while (pn != nullptr);
	cout << ss.str() << endl;
}

template<typename IntType, typename ValType>
void XFT<IntType, ValType>::Insert(IntType key, ValType val)
{
	// Create layer 0 node and find its predecessor and successor
	XFTNode<IntType, ValType>* base_node = new XFTNode<IntType, ValType>();
	base_node->key = key;
	base_node->val = val;
	XFTNode<IntType, ValType>* pre = PredNode(key);
	XFTNode<IntType, ValType>* succ = SuccNode(key);
	XFTNode<IntType, ValType>* parent = &Root;


	// Insert the new node into the linked list
	pre->succ = base_node;
	base_node->pred = pre;
	succ->pred = base_node;
	base_node->succ = succ;
	base_node->desc = base_node;
	Hash[0].insert(make_pair(key, base_node));
	

	// Wikipedia: "Next, we walk from the root to the new leaf, creating the
	// necessary nodes on the way down, inserting them into the respective
	// hash tables and updating descendant pointers where necessary."
	bool isLeftChild = false;
	IntType prefix = 0;
	typename std::unordered_map<IntType, XFTNode<IntType, ValType>*>::const_iterator got = Hash[0].begin();
	for (int i = LayerCount - 1; i >= 0; i--)
	{
		prefix = GetPrefix(key, i);
		isLeftChild = (prefix % 2 == 0);

		// Does current layer contain the prefix? If not add it.
		got = Hash[i].find(prefix);
		if (got == Hash[i].end())
		{
			// Node doesn't exist, create prefix node
			XFTNode<IntType, ValType>* prefix_node = new XFTNode<IntType, ValType>();
			prefix_node->key = prefix;
			prefix_node->desc = base_node;

			// Add node to parent
			if (isLeftChild)
			{
				parent->lc = prefix_node;
			}
			else
			{
				parent->rc = prefix_node;
			}

			// Add or remove parent desc. pointer if necessary
			if (parent->lc != nullptr && parent->rc != nullptr)
			{
				parent->desc = nullptr;
			}
			else
			{
				parent->desc = base_node;
			}

			// Add prefix node to hash
			Hash[i].insert(make_pair(prefix, prefix_node));

			// Set prefix node as new parent
			parent = prefix_node;
		}
		else // node already exists
		{
			XFTNode<IntType, ValType>* existing_node = got->second;

			// Add node to parent
			if (isLeftChild)
			{
				parent->lc = existing_node;
			}
			else
			{
				parent->rc = existing_node;
			}

			if (parent->lc != nullptr && parent->rc != nullptr)
			{
				parent->desc = nullptr;
			}
			else
			{
				if (isLeftChild)
				{
					if (parent->desc == base_node->pred) parent->desc = base_node;
				}
				else
				{
					if (parent->desc == base_node->succ) parent->desc = base_node;
				}
			}

			// Set already existing node as new parent
			parent = existing_node;
		}
	}	
}

template<typename IntType, typename ValType>
void XFT<IntType, ValType>::Delete(IntType key)
{
	typename std::unordered_map<IntType, XFTNode<IntType, ValType>*>::iterator got = Hash[0].begin();
	bool canDelete = true;
	bool isLeftChild;
	XFTNode<IntType, ValType> *k, *p, *s, *parent;
	IntType prefix, childPrefix;
	childPrefix = key;

	// If key doesn't exist there's nothing to do.
	got = Hash[0].find(key);
	if (got == Hash[0].end())
		return;

	// Get layer 0 node k, get its pred und succ nodes p and s, remove k from
	// linked list, mend linked list.
	k = got->second;
	p = k->pred;
	s = k->succ;
	p->succ = s;
	s->pred = p;
	Hash[0].erase(key);
	

	// Move up through the hash layers, find each parent node, adjust child and
	// descendant pointers. We delete nodes until we find the first node with
	// two children. After that we can no longer delete any node.
	for (int i = 1; i < LayerCount; i++)
	{
		prefix = GetPrefix(key, i);
		got = Hash[i].find(prefix);
		isLeftChild = (childPrefix % 2 == 0);
		parent = got->second;

		if (parent->lc == nullptr || parent->rc == nullptr)
		{
			// Parent has exactly one child
			if (canDelete)
			{
				Hash[i].erase(prefix);
				delete parent;
			}
			else
			{
				// Fix the descendant pointer if necessary
				if (parent->desc == k)
				{
					if (parent->key == GetPrefix(s->key, i))
					{
						parent->desc = s;
					}
					else
					{
						parent->desc = p;
					}
				}
			}
		}
		// End exactly one child
		else
		{
			// Parent has two children.
			// If we are still in "delete mode" we have to null the correct
			// child pointer.
			if (canDelete)
			{
				canDelete = false;
				// Which child are we?
				if (isLeftChild)
				{
					// null the left child pointer
					parent->lc = nullptr;
				}
				else
				{
					// null the right child pointer
					parent->rc = nullptr;
				}
				// Fix the descendant pointer if necessary
				if (parent->desc == k || parent->desc == nullptr)
				{
					if (parent->key == GetPrefix(s->key, i))
					{
						parent->desc = s;
					}
					else
					{
						parent->desc = p;
					}
				}
			}
		}
		childPrefix = prefix;
	} // end for loop

	// take care of Root
	isLeftChild = (childPrefix % 2 == 0);
	if (canDelete)
	{
		if (isLeftChild)
		{
			Root.lc = nullptr;
		}
		else
		{
			Root.rc = nullptr;
		}

		if (Root.lc == nullptr && Root.rc == nullptr)
		{
			Root.desc = nullptr;
		}
	}
	if (Root.desc == k || Root.desc == nullptr)
	{
		if (s != &Maximum)
		{
			Root.desc = s;
		}
		else if (p != &Minimum)
		{
			Root.desc = p;
		}
	}

	// Finally delete the node
	delete k;
}

template<typename IntType, typename ValType>
bool XFT<IntType, ValType>::Contains(IntType key)
{
	typename std::unordered_map<IntType, XFTNode<IntType, ValType>*>::const_iterator got = Hash[0].find(key);
	if (got == Hash[0].end())
		return false;
	else
		return true;
}

template<typename IntType, typename ValType>
pair<IntType, ValType> XFT<IntType, ValType>::Pred(IntType key)
{
	// Empty structure has Minimum as predecessor
	if (Hash[0].size() == 0) return make_pair(-1, nullptr);

	// TODO: return nullptr if pred is Minimum!
	XFTNode<IntType, ValType>* pn = PredNode(key);
	pair<IntType, ValType> result = make_pair(pn->key, pn->val);
	return result;
}

template<typename IntType, typename ValType>
pair<IntType, ValType> XFT<IntType, ValType>::Succ(IntType key)
{
	// Empty structure has Maximum as predecessor
	if (Hash[0].size() == 0) return make_pair(0, nullptr);

	// TODO: return nullptr if succ is Maximum!
	XFTNode<IntType, ValType>* sn = SuccNode(key);
	pair<IntType, ValType> result = make_pair(sn->key, sn->val);
	return result;
}

template<typename IntType, typename ValType>
std::pair<IntType, ValType> XFT<IntType, ValType>::Find(IntType)
{
	// TODO: Implement me!
	return std::pair<IntType, ValType>();
}

template<typename IntType, typename ValType>
XFTNode<IntType, ValType>* XFT<IntType, ValType>::PredNode(IntType key)
{
	// Empty structure has Minimum as predecessor
	if (Hash[0].size() == 0) return &Minimum;

	XFTNode<IntType, ValType>* candidate = XFT::PredOrSucc(key);
	//if (candidate == nullptr) cout << "Null pointer!" << endl;
	if (candidate->key <= key)
	{
		return candidate;
	}
	else
	{
		return candidate->pred;
	}
}

template<typename IntType, typename ValType>
XFTNode<IntType, ValType>* XFT<IntType, ValType>::SuccNode(IntType key)
{
	// Empty structure has Maximum as successor
	if (Hash[0].size() == 0) return &Maximum;

	XFTNode<IntType, ValType>* candidate = XFT::PredOrSucc(key);

	if (candidate->key >= key)
		return candidate;
	else
		return candidate->succ;
}

template<typename IntType, typename ValType>
XFTNode<IntType, ValType>* XFT<IntType, ValType>::PredOrSucc(IntType key)
{
	XFTNode<IntType, ValType>* candidate = &Root;
	//unordered_map<IntType,XFTNode<IntType>>::const_iterator got;
	typename std::unordered_map<IntType, XFTNode<IntType, ValType>*>::const_iterator got = Hash[0].begin();
	int bottom = 0;
	int ceiling = LayerCount - 1;
	int current = 0;
	IntType prefix;

	// Binary search on Hash layers
	while (bottom <= ceiling)
	{
		current = bottom + (ceiling - bottom) / 2;
		prefix = XFT::GetPrefix(key, current);
		got = Hash[current].find(prefix);

		if (got != Hash[current].end())
		{
			// node already exists
			// search in the area below
			candidate = got->second;
			ceiling = current - 1;
		}
		else
		{
			// node doesn't exist
			// search in the area above
			bottom = current + 1;
		}
	}
	return candidate->desc;
}

template<typename IntType, typename ValType>
size_t XFT<IntType, ValType>::size()
{
	return Hash[0].size();
}

template<typename IntType, typename ValType>
IntType XFT<IntType, ValType>::GetPrefix(IntType key, IntType layer)
{
	//return key >> (32-bits);
	return key >> layer;
}
