#pragma once

template<typename IntType>
struct avEBNode
{
	IntType minimum;
	IntType maximum;
	IntType u;
	avEBNode<IntType> **cluster; // array of size sqrt(u) of vEBNodes 
	avEBNode<IntType> *summary = nullptr; // single vEBNode (possible root of a tree)
	//std::unordered_map<IntType, vEBNode<IntType>*> h_cluster;
	//bool deleted = false;
};
#pragma once
