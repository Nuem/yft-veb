//#include <vector>
#include <algorithm>
#include "VecBucket.hpp"
#include "XFT.hpp"

using namespace std;

template<typename IntType>
class YFT
{
	public:
		YFT();
		~YFT();
		void Clear();
		void Insert(IntType);
		void Delete(IntType);
		bool Contains(IntType);
		void SetBucketSize(IntType min, IntType max);
		IntType Find(IntType);
		IntType Pred(IntType);
		IntType Succ(IntType);
		IntType Size();
	private:
		XFT<IntType, VecBucket<IntType>*> xft;
		int bmode; // bucket mode
		int bmin;
		int bmax;
        VecBucket<IntType>* MakeBucket();
		VecBucket<IntType>* SuccBucket(IntType key);
        int IntSize;
        IntType m_size = 0;
};

template<typename IntType>
YFT<IntType>::YFT()
{
	IntSize = sizeof(IntType) * 8;
	bmax = 2 * IntSize;
	bmin = IntSize / 2;
}

template<typename IntType>
YFT<IntType>::~YFT()
{
	Clear();
}

template<typename IntType>
void YFT<IntType>::Clear()
{
	xft.Clear();
	m_size = 0;
}

template<typename IntType>
void YFT<IntType>::Insert(IntType key)
{
	if (Contains(key)) return;

	m_size++;
	VecBucket<IntType>* bucket = SuccBucket(key);

	// nullptr means we don't have any buckets
	// make new bucket and add it to the xft
	if (bucket == nullptr)
	{
		bucket = MakeBucket();
		xft.Insert(key, bucket);
	}

	bucket->Add(key);

	// if the bucket is too large split it, remove old bucket from xft,
	// delete old bucket, add new buckets to xft
	if (bucket->Size() > bmax)
	{
		// Split bucket, remove old bucket, add new buckets.
		xft.Delete(bucket->GetRep());
		VecBucket<IntType>* split = bucket->Split(bmax / 2);
		xft.Insert(bucket->GetRep(), bucket);
		xft.Insert(split->GetRep(), split);
	}
}

template<typename IntType>
void YFT<IntType>::Delete(IntType key)
{
	if (!Contains(key)) return;
	
	XFTNode<IntType, VecBucket<IntType>*>* succ_node = xft.SuccNode(key);
	VecBucket<IntType>* succ_bucket = succ_node->val;
	VecBucket<IntType>* pred_bucket = succ_node->pred->val;

	succ_bucket->Remove(key);
	pred_bucket->Remove(key);

	m_size--;
}

template<typename IntType>
bool YFT<IntType>::Contains(IntType key)
{
	XFTNode<IntType, VecBucket<IntType>*>* succ_node = xft.SuccNode(key);
	//if (succ_node == &(xft.Maximum)) return false;

	VecBucket<IntType>* bucket = succ_node->val;
	if (bucket != nullptr)
	{
		if (bucket->Contains(key)) return true;
	}
	bucket = (succ_node->pred)->val;
	if (bucket != nullptr)
	{
		if (bucket->Contains(key)) return true;
	}
	return false;
}

template<typename IntType>
void YFT<IntType>::SetBucketSize(IntType min, IntType max)
{
	bmin = min;
	bmax = max;
}

template<typename IntType>
IntType YFT<IntType>::Find(IntType key)
{
	if (Contains(key)) return key;
	return 0;
}

template<typename IntType>
IntType YFT<IntType>::Pred(IntType key)
{
	IntType pred1 = 0, pred2 = 0;
	XFTNode<IntType, VecBucket<IntType>*>* pred_node = xft.PredNode(key);
	VecBucket<IntType>* pred_bucket = pred_node->val;
	VecBucket<IntType>* succ_bucket = pred_node->succ->val;
	if (pred_bucket != nullptr) pred1 = pred_bucket->Succ(key);
	if (succ_bucket != nullptr) pred2 = succ_bucket->Succ(key);
	return (pred1 > pred2) ? pred1 : pred2;
}

template<typename IntType>
IntType YFT<IntType>::Succ(IntType key)
{
	IntType succ1 = -1, succ2 = -1;
	XFTNode<IntType, VecBucket<IntType>*>* succ_node = xft.SuccNode(key);
	VecBucket<IntType>* succ_bucket = succ_node->val;
	VecBucket<IntType>* pred_bucket = succ_node->pred->val;
	if (succ_bucket != nullptr) succ1 = succ_bucket->Succ(key);
	if (pred_bucket != nullptr) succ2 = pred_bucket->Succ(key);
	return (succ1 < succ2) ? succ1 : succ2;
}

template<typename IntType>
VecBucket<IntType>* YFT<IntType>::SuccBucket(IntType key)
{
	IntType succ1 = -1, succ2 = -1;
	XFTNode<IntType, VecBucket<IntType>*>* succ_node = xft.SuccNode(key);
	VecBucket<IntType>* succ_bucket = succ_node->val;
	VecBucket<IntType>* pred_bucket = succ_node->pred->val;
	if (succ_bucket != nullptr) succ1 = succ_bucket->Succ(key);
	if (pred_bucket != nullptr) succ2 = pred_bucket->Succ(key);
	return (succ1 < succ2) ? succ_bucket : pred_bucket;
}

template<typename IntType>
IntType YFT<IntType>::Size()
{
	// TODO: Implement me!
	return IntType();
}

template<typename IntType>
VecBucket<IntType>* YFT<IntType>::MakeBucket()
{
	VecBucket<IntType>* bt = new VecBucket<IntType>(bmin, bmax);
	return bt;
}
