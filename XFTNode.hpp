#pragma once

template<typename IntType, typename ValType>
struct XFTNode
{
	XFTNode<IntType, ValType> *pred = nullptr;
	XFTNode<IntType, ValType> *succ = nullptr;
	XFTNode<IntType, ValType> *desc = nullptr;
	XFTNode<IntType, ValType> *lc = nullptr;
	XFTNode<IntType, ValType> *rc = nullptr;
	IntType key;
	ValType val;
};
